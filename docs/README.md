# Examples

# Exponentially weighted moving average

```rust
use matplotrust::*;

let mut samples: Vec<f64> = Vec::new();
let mut rng = rand::thread_rng();
samples.push(10.0);
let mut ewma = EWMA::new(10.0, 25);
let mut average: Vec<f64> = Vec::new();
average.push(10.0);
for i in 1..100 {
    let prev = samples[i-1];
    samples.push(prev + rng.gen_range(-2.0, 2.0));
    average.push(ewma.update(samples[i]));
}
print!("{:?}", average);
let mut figure = Figure::new();
let points = scatter_plot::<i64, f64>((0..100).collect(), samples, None);
figure.add_plot(points);
let mut options = LinePlotOptions::new();
options.colour = Some("red".to_string());
let line = line_plot::<i64, f64>((0..100).collect(), average, Some(options));
figure.add_plot(line);
let s = figure.save("./docs/figures/ewma.png", None);
```
![](figures/ewma.png)

# Monotonic Cubic Spline interpolation

```rust
 let x = vec![0.0, 2.0, 3.0, 10.0];
let y = vec![1.0, 4.0, 8.0, 10.5];

let mut spline = MonotonicCubicSpline::new(&x, &y);

let mut x_interp = Vec::new();
let mut y_interp = Vec::new();
for i in 0..100 {
    let p = i as f64 / 10.0;
    x_interp.push(p);
    y_interp.push(spline.interpolate(p));
}
```

![](figures/monotonic_cubic_spline.png)