pub fn squared_distance(x: &Vec<f64>, y: &Vec<f64>) -> f64 {
    assert_eq!(x.len(), y.len(), "Vectors must have the same size.");

    let mut sum = 0.0;
    for i in 0..x.len() {
        sum += (x[i] - y[i]).powi(2)
    }

    return sum;
}