#[cfg(test)]
extern crate rand;

struct EWMA {
    current: f64,
    age: i64,
    decay: f64
}

impl EWMA {

 pub fn new(value: f64, age: i64) -> EWMA {
    let decay = 2.0 / (age as f64 + 1.0);
     return EWMA {
         age: age,
         decay: decay,
         current: value
     };
 }

 pub fn update(&mut self, value : f64) -> f64 {
    self.current = (value * self.decay) + (self.current * (1.0 - self.decay));
    return self.current;
 }
}

#[cfg(test)]
mod test_ewma {

    use super::*;
    use rand::Rng;

    #[test]
    fn ewma_test() {

        use matplotrust::*;

        let mut samples: Vec<f64> = Vec::new();
        let mut rng = rand::thread_rng();
        samples.push(10.0);
        let mut ewma = EWMA::new(10.0, 25);
        let mut average: Vec<f64> = Vec::new();
        average.push(10.0);
        for i in 1..100 {
            let prev = samples[i-1];
            samples.push(prev + rng.gen_range(-2.0, 2.0));
            average.push(ewma.update(samples[i]));
        }
        print!("{:?}", average);
        let mut figure = Figure::new();
        let points = scatter_plot::<i64, f64>((0..100).collect(), samples, None);
        figure.add_plot(points);
        let mut options = LinePlotOptions::new();
        options.colour = Some("red".to_string());
        let line = line_plot::<i64, f64>((0..100).collect(), average, Some(options));
        figure.add_plot(line);
        let s = figure.save("./docs/figures/ewma.png", None);
    }
}