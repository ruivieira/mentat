* 0.0.3
  * Add Monotonic Cubic Spline interpolation

* 0.0.2
  * Add `pdf` and `logpdf` to the Normal distribution