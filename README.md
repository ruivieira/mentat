# mentat

A Rust library for scientific computing

# features

* Exponentially weighted moving average (EWMA)
* Monotonic Cubic Spline interpolation


# examples

Full examples are available [here](docs/README.md).